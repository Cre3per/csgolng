const https = require('https');
const querystring = require('qs');

class Web
{
  getURLHost(url)
  {
    // < prot://sub.domain.com/remote/path
    // < sub.domain.com/remote/path
    // < sub.domain.com
    // > sub.domain.com

    let start = 0;
    let end = 0;

    let slash = url.indexOf('/');
    let dot = url.indexOf('.');

    if (dot === -1)
    {
      throw { err: `${url} is not a URL` };
    }

    if (slash < dot)
    {
      if (slash !== -1)
      {
        start = slash + 2;
      }
    }

    slash = url.indexOf('/', start);

    if (slash === -1)
    {
      end = url.length;
    }
    else
    {
      end = slash;
    }

    return url.substr(start, end - start);
  }

  getURLPath(url)
  {
    // < prot://sub.domain.com/remote/path
    // > /remote/path

    // < sub.domain.com
    // > /

    let start = 0;

    let dot = url.indexOf('.');

    if (dot === -1)
    {
      throw { err: `${url} is not a URL` };
    }

    let slash = url.indexOf('/', dot);

    if (slash === -1)
    {
      return '/';
    }
    else
    {
      return url.substr(slash);
    }
  }

  /**
   * @callback Web~requestCallback
   * @param {object} err An error object or undefined
   * @param {string} data The data returned
   */
  /**
   * 
   * @param {string} url
   * @param {object} headers Where cookie is an object, not a string. Content-Type and Content-Length will be set.
   * @param {Web~requestCallback} callback 
   * @param {string} data Form data
   */
  request(url, headers, callback, data = undefined)
  {
    let isPost = (data !== undefined);

    let cookieString = '';
    
    let optionHeaders = (headers === undefined) ? { } : JSON.parse(JSON.stringify(headers));

    if ((headers !== undefined) && (headers.cookie !== undefined))
    {
      for (let key in headers.cookie)
      {
        cookieString += `${key}=${headers.cookie[key]};`;
      }

      optionHeaders.cookie = cookieString;
    }

    let options =
    {
      hostname: this.getURLHost(url),
      path: this.getURLPath(url),
      method: isPost ? 'POST' : 'GET',
      headers: optionHeaders
    };

    if (isPost)
    {
      options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      options.headers['Content-Length'] = Buffer.byteLength(data);
    }

    let req = https.request(options, (res) =>
    {
      let rawData = '';
      
      res.on('data', (data) =>
      {
        rawData += data;
      });

      res.on('end', () =>
      {
        callback(undefined, { data: rawData, res: res });
      });
    });
    
    req.on('error', (err) =>
    {
      callback(err);
    });

    if (isPost)
    {
      req.write(data);
    }

    req.end();
  }

  /**
   * @callback Web~getCallback
   * @param {object} err An error object or undefined
   * @param {string} data The data returned
   */
  /**
   * 
   * @param {string} url
   * @param {object} headers
   * @param {Web~getCallback} callback 
   */
  get(url, headers, callback)
  {
    this.request(url, headers, callback);
  }

  /**
   * @callback Web~postCallback
   * @param {object} err An error object or undefined
   * @param {string} data The data returned
   */
  /**
   * 
   * @param {string} url
   * @param {object} headers
   * @param {string} data
   * @param {Web~postCallback} callback 
   */
  post(url, headers, data, callback)
  {
    this.request(url, headers, callback, data);
  }

  /**
   * @callback Web~getJSONCallback
   * @param {object} err An error object or undefined
   * @param {object} data The JSON object as returned by the url
   */
  /**
   * Sends a GET request to @url and parses the result to a JSON object
   * @param {string} url 
   * @param {object} headers
   * @param {Web~getJSONCallback} callback 
   */
  getJSON(url, headers, callback)
  {
    this.get(url, headers, (err, data) =>
    {
      if (err !== undefined)
      {
        callback(err);
      }
      else
      {
        let jsonData = { };

        try
        {
          jsonData = JSON.parse(data.data);
        }
        catch (ex)
        {
          callback(ex);
        }

        callback(undefined, jsonData);
      }
    });
  }

  /**
   * @callback Web~postJSONCallback
   * @param {object} err An error object or undefined
   * @param {object} data The JSON object as returned by the url
   */
  /**
   * Sends a POST request to @url and parses the result to a JSON object
   * @param {string} url
   * @param {object} headers
   * @param {string} data
   * @param {Web~postJSONCallback} callback 
   */
  postJSON(url, headers, data, callback)
  {
    this.post(url, headers, data, (err, data) =>
    {
      if (err !== undefined)
      {
        callback(err);
      }
      else
      {
        let jsonData = { };

        try
        {
          jsonData = JSON.parse(data.data);
        }
        catch (ex)
        {
          callback(ex);
        }

        callback(undefined, jsonData);
      }
    });
  }
}

module.exports = new Web();