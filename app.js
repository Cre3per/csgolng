const userIO = require('./userio.js');
const commands = require('./commands.js');

class App
{
  run()
  {
    userIO.print('do: ');
    
    userIO.readln((text) =>
    {
      commands.call(text, () =>
      {
        this.run();
      });
    });
  }

  constructor()
  {
    this.run();
  }
}

new App();