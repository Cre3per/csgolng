const readline = require('readline');

const rl = readline.createInterface(
{
  input: process.stdin,
  output: process.stdout
});

class UserIO
{
  /**
   * @callback
   * @name Console~inCallback
   * @param text
   */
  /**
   * Reads console input
   * @param {Console~inCallback} callback 
   */
  readln(callback)
  {
    rl.question('', (text) =>
    {
      callback(text);
    });
  }

  // OBSOLETE
  print(text)
  {
    this.println(text);
  }
  
  println(text)
  {
    console.log(text);
  }

  /**
   * Default constructor
   */
  constructor()
  {

  }
}

module.exports = new UserIO();