const TradePoll = require('./tradepoll.js');
const Item = require('./item.js');

const web = require('../web.js');
const userIO = require('../userio.js');

class CSGOLoungeSearch
{
  /**
   * 
   * @param {string} html 
   * @return {(Item|array)}
   */
  extractFormItems(html)
  {
    let items = [ ];

    let start = 0;
    let end = -1;

    userIO.println(`START EXTR`);

    do
    {
      start = html.indexOf('<div class=\"oitm\">', start);

      if (start !== -1)
      {
        let item = new Item();

        start += 18;

        start = html.indexOf('<b>', start) + 3;
        userIO.println(`B ${start}`);
        end = html.indexOf('</b>', start);

        item.market_name = html.substring(start, end);
        
        start = html.indexOf('<i>', end);

        if (start !== -1)
        {
          start += 3;

          end = html.indexOf('</i>', start);

          item.rarity = html.substring(start, end).trim();
        }

        // TODO: Assign id

        items.push(item);

        start = end;
      }

    } while (start !== -1)

    return items;
  }

  /**
   * 
   * @param {string} html 
   * @return {object} { pageCount, (TradePoll|array) }
   */
  extractTradePolls(html)
  {
    let polls = [ ];

    let pageCount = 0;

    let start = html.lastIndexOf('<li><a href=\"/result');
    let end = -1;

    if (start !== -1)
    {
      start = html.indexOf('>', start) + 1;
      end = html.indexOf('<', start);

      pageCount = parseInt(html.substring(start, end));
    }

    start = 0;
    end = -1;

    do
    {
      start = html.indexOf('<div class=\"tradepoll\">', start);

      if (start !== -1)
      {
        if (pageCount === 0)
        {
          pageCount = 1;
        }

        let poll = new TradePoll();

        start += 23;
        
        start = html.indexOf('title=\"', start);

        if (start != -1)
        {
          start += 7;

          end = html.indexOf('\">', start);

          poll.title = html.substring(start, end);
        }

        start = html.indexOf('<a href=\"trade?t=', start) + 17;
        end = html.indexOf('\"', start);

        poll.id = parseInt(html.substring(start, end));

        start = html.indexOf('<b>', end) + 3;
        end = html.indexOf('</b>', start);

        poll.userNick = html.substring(start, end);

        start = html.indexOf('<form class=\"left\">', end) + 21;
        end = html.indexOf('</form>', start);


        userIO.println(`R ${start} ${end}`);
        poll.litems = this.extractFormItems(html.substring(start, end));
        
        start = html.indexOf('<form class=\"right\">', end) + 21;
        end = html.indexOf('</form>', start);

        
        userIO.println(`A ${start} ${end}`);
        poll.ritems = this.extractFormItems(html.substring(start, end));
        userIO.println(`B`);

        polls.push(poll);

        start = end;
      }

    } while (start !== -1);

    return polls;
  }

  /**
   * @callback
   * @name CSGOLoungeSearch~searchCallback
   * @param {object} err An error object or undefined
   * @param {(TradePoll|array)} data
   */

  /**
   * 
   * @param {(number|array)} indexes 
   * @param {number} page
   */
  search(indexes, page, callback)
  {
    let query = '';

    for (let index of indexes)
    {
      query += `&ldef_index%5B0%5D=${index}&lquality%5B0%5D=0`;
    }

    userIO.println('GET');
    web.get(`https://csgolounge.com/result?${query}&p=${page}`, undefined, (err, data) =>
    {
      userIO.println(`GOT`);

      if (err)
      {
        userIO.println(`unable to get items (page ${page})`);
      }
      else
      {
        userIO.println(`EXT`);

        let polls = this.extractTradePolls(data.data);

        userIO.println(`POLLS`);
        userIO.println(polls);
      }

      callback(undefined, undefined);
    });
  }

  /**
   * 
   * @param {(number|array)} indexes
   */
  search(indexes, callback)
  {

  }

  constructor()
  {

  }
}

module.exports = new CSGOLoungeSearch();