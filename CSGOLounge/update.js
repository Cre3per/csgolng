const Item = require('./item.js');

const userIO = require('../userio.js');
const web = require('../web.js');

const fs = require('fs');



class CSGOLoungeUpdate
{
  extractPageInfo(html)
  {
    let pageCount = 0;
    let items = [ ];

    let start = html.lastIndexOf('\">') + 2;
    let end = html.indexOf('<', start);

    pageCount = parseInt(html.substr(start, end - start));

    let itemStart = -1;

    do
    {
      itemStart = html.indexOf('<div class=\"name\">', itemStart);

      if (itemStart != -1)
      {
        let item = new Item();

        start = html.indexOf('<b>', itemStart) + 3;
        end = html.indexOf('</b>', start);

        item.market_name = html.substr(start, end - start);
        
        start = html.indexOf('<i>', end);

        if ((start != -1) && (start < (end + 100)))
        {
          start += 3;

          end = html.indexOf('</i>', start);

          item.rarity = html.substr(start, end - start).trim();
        }

        start = html.indexOf('value=\"', end) + 7;
        end = html.indexOf('\"', start);

        item.index = parseInt(html.substr(start, end - start));

        items.push(item);

        itemStart = end;
      }

    } while (itemStart != -1)

    return {
      pageCount,
      items
    };
  }

  /**
   * 
   * @param {number} page Page number
   * @param {*} callback 
   */
  getItemPage(page, callback)
  {
    web.post(`https://csgolounge.com/ajax/tradeCsRight.php`, undefined, `type=1&search=1&page=${page}&fraze`, (err, data) =>
    {
      callback(err, this.extractPageInfo(data.data));
    });
  }

  /**
   * 
   * @param {number} page The current page's number
   * @param {object} err An error object or undefined
   * @param {object} data { pageCount, items }
   * @param {*} callback
   */
  collectItemDefinitions(page, err, data, callback)
  {
    if (err)
    {
      userIO.println(`unable to update item definitions (page ${page})`);
      userIO.println(err);
    }
    else
    {
      if (!this.totalItemDefinitionPageCount)
      {
        this.totalItemDefinitionPageCount = data.pageCount;
      }

      this.collectedItemDefinitions = this.collectedItemDefinitions.concat(data.items);
    }

    if (++this.processedItemDefinitionPageCount >= this.totalItemDefinitionPageCount)
    {
      callback && callback();
    }
  }

  /**
   * Clears existing item definitions from defs.json and {@link collectedItemDefinitions}
   */
  clearItemDefinitions(callback)
  {
    fs.writeFile(`${__dirname}/defs.json`, '[ ]', (err) =>
    {
      if (err)
      {
        userIO.println(`Unable to clear defs.json:`);
        userIO.println(err);
      }

      this.itemDefinitions = [ ];
      this.totalItemDefinitionPageCount = 0;
      this.processedItemDefinitionPageCount = 0;

      callback && callback();
    });
  }

  saveItemDefinitionsToDisk(callback)
  {
    fs.writeFile(`${__dirname}/defs.json`, JSON.stringify(this.collectedItemDefinitions), (err) =>
    {
      callback(err);
    });
  }

  /**
   * @callback
   * @name CSGOLoungeUpdate~itemDefinitionsCallback
   * @param {object} err An error object or undefined
   * @param {object} data { successful, total }
   */
  /**
   * 
   * @param {*} callback 
   */
  itemDefinitions(callback)
  {
    this.clearItemDefinitions(() =>
    {
      let items = [ ];

      let successful = 0;

      this.getItemPage(1, (err, data) =>
      {
        this.collectItemDefinitions(1, err, data);

        if (!err) ++successful;

        for (let i = 2; i <= this.totalItemDefinitionPageCount; ++i)
        {
          this.getItemPage(i, (err, data) =>
          {
            if (!err) ++successful;

            this.collectItemDefinitions(i, err, data, () =>
            {
              this.saveItemDefinitionsToDisk((err) =>
              {
                callback(err, { successful: successful, total: this.totalItemDefinitionPageCount });
              });
            });
          });
        }
      });
    });
  } 

  /*  *
   * Default constructor
   */
  constructor()
  {
    this.collectedItemDefinitions = [ ];

    this.totalItemDefinitionPageCount = 0;
    this.processedItemDefinitionPageCount = 0;
  }
}

module.exports = new CSGOLoungeUpdate();