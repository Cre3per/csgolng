

class TradePoll
{
  constructor()
  {
    /**
     * @type {string}
     */
    this.title = undefined;

    /**
     * @type {number}
     */
    this.id = undefined;
    
    /**
     * @type {string}
     */
    this.userNick = undefined;
    
    /**
     * @type {(Item|array)}
     */
    this.litems = undefined;
    
    /**
     * @type {(Item|array)}
     */
    this.ritems = undefined;
  }
}

module.exports = TradePoll;