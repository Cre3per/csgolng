const web = require('../web.js');
const userIO = require('../userio.js');

const update = require('./update.js');
const search = require('./search.js');

class CSGOLounge
{

  /**
   * Default constructor
   */
  constructor()
  {
    this.update = update;
    this.search = search;
  }
}

module.exports = new CSGOLounge();