const userIO = require('./userio.js');
const lng = require('./CSGOLounge/csgolng.js');


class Commands
{
  
  exit(args, callback)
  {
    userIO.println(args);
    process.exit(0);
  }

  help(args, callback)
  {
    userIO.println('Available commands:');

    for (let command of this.commands)
    {
      let line = '';

      for (let alias of command.aliases)
      {
        line += alias + ' ';
      }

      userIO.println(line);
    }
    
    callback && callback();
  }

  update(args, callback)
  {
    lng.update.itemDefinitions((err, data) =>
    {
      userIO.println(`Updated ${data.successful}/${data.total} item definitions`);
      
      callback && callback();
    });
  }

  search(args, callback)
  {
    userIO.println(`Searching for ${JSON.stringify(args)}`);

    lng.search.search(args.map((s) => { return parseInt(args); }), 1, (err, data) =>
    {
      userIO.println(`Search finished`);
      userIO.println(data);

      callback && callback();
    });
  }


  call(commandline, callback)
  {
    let split = commandline.split(' ');
    
    let command = split[0];
    let args = split.slice(1, split.length);

    for (let cmd of this.commands)
    {
      if (cmd.aliases.includes(command))
      {
        cmd.fn(args, callback);

        return;
      }
    }

    userIO.println(`no such command ${command}, type 'help'`);
    callback && callback();
  }

  registerCommand(aliases, fn)
  {
    this.commands.push({ aliases, fn: fn.bind(this) });
  }

  constructor()
  {
    this.commands = [ ];

    this.registerCommand([ 'exit', 'q' ], this.exit);
    this.registerCommand([ 'help', '?' ], this.help);
    this.registerCommand([ 'search', 's' ], this.search);
    this.registerCommand([ 'update' ], this.update);
  }
}

module.exports = new Commands();